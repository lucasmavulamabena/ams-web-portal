import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsComponentComponent } from './ams-component.component';

describe('AmsComponentComponent', () => {
  let component: AmsComponentComponent;
  let fixture: ComponentFixture<AmsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
