import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AmsComponentComponent } from './ams-component/ams-component.component';
import { TenaneAppliancesComponent } from './tenane-appliances/tenane-appliances.component';
import {
  MatSidenavModule,
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    AmsComponentComponent,
    TenaneAppliancesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    RouterModule.forRoot([
      { path: '', component: AmsComponentComponent },
      { path: 'TenantAppliances/:tenantId', component: TenaneAppliancesComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
