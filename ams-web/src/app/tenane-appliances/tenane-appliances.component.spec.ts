import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenaneAppliancesComponent } from './tenane-appliances.component';

describe('TenaneAppliancesComponent', () => {
  let component: TenaneAppliancesComponent;
  let fixture: ComponentFixture<TenaneAppliancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenaneAppliancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenaneAppliancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
